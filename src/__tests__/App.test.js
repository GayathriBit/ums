import React from 'react';
import ReactDOM from 'react-dom';
import App from '.././App';

it('should add two numbers', () => {
    expect(1+2).toBe(3);
  });


it('should subtract two numbers', () => {
    expect(211-1).toBe(210);
  });


it('should multiply two numbers', () => {
    expect(555*1).toBe(555);
  });
it('should divide two numbers', () => {
    expect(555/1).toBe(555);
  });
